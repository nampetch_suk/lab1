/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryMethod;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Room107
 */
public class MySQLDBConnection implements DBConnection {
    private Connection connection;
    
    public MySQLDBConnection(String  host,String port, String dbName, String user,String password){
       
        try {
            Class.forName("com.mysql.jdbc.Driver");
            
            String url = "jdbc:mysql://"
                    + host + ":" + port + "/" +dbName;
            connection = DriverManager.getConnection(url,user,password);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
     
    }
 
    @Override
    public ResultSet executeQuery(String query) {
           
        try {
            Statement st = connection.createStatement();
            ResultSet results = st.executeQuery(query);
            return results;
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    @Override
    public void update(String command){
        try {
            Statement st = connection.createStatement();
            st.executeUpdate(command);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void close(){
        
        
            try {
                if(connection != null){
                connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(MySQLDBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
}


    

