/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryMethod;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Room107
 */
public class testDBConnectionFactory {
    public static void main(String[] args) throws SQLException{
        //MySQLDBConnection sql = new MySQLDBConnection("www.db4free.net","3306","cs386cstu","cs386user","rongviri");
        DBConnectionFactory factory = new DBConnectionFactory();
        //Set File "DB.properties" ที่กำหนดชื่อยี่ห้อฐานข้อมูล ชื่อฐานข้อมูล
        //PortNumber User Name,Password
        DBConnection connection = factory.createDBConnectrion("mysql", "www.db4free.net", "3306", "cs285name", "cs285user", "nampetch");
        
        if(connection != null){
            ResultSet results = connection.executeQuery("select * from Flight;");
            while(results.next()){
                System.out.println("From:"+results.getString(1));
                    
                System.out.println("To:"+results.getString(2));
            }
        }
        
    }
}
