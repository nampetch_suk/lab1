/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.domain;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class CostDetail implements Serializable {
    public double OutboundPrice;
    public String OutboundCostName;

    public String getOutboundCostName() {
        return OutboundCostName;
    }

    public void setOutboundCostName(String OutboundCostName) {
        this.OutboundCostName = OutboundCostName;
    }
    public double getOutboundPrice() {
        return OutboundPrice;
    }

    public void setOutboundPrice(double OutboundPrice) {
        this.OutboundPrice = OutboundPrice;
    }
    

    
}
