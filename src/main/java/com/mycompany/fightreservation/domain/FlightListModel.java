/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.domain;

import java.util.ArrayList;
import javax.swing.AbstractListModel;

/**
 *
 * @author Administrator
 */
public class FlightListModel extends AbstractListModel<FlightsDetail>{

    private ArrayList<FlightsDetail> model;
    public FlightListModel(){
        model = new ArrayList<FlightsDetail>();
    }
    
    public ArrayList<FlightsDetail> getFlightArray(){
        return model;
    }
    
    public FlightListModel(ArrayList<FlightsDetail> model){
        this.model = model;
    }
    
    @Override
    public int getSize() {
        return model.size();
    }

    @Override
    public FlightsDetail getElementAt(int index) {
        return model.get(index);
    }
   
}
