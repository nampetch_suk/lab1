/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.domain;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class FlightsDetail implements Serializable,Comparable<FlightsDetail> {
    public double OutboundFlightPrice;
    public String OutboundAirline;
    public String TakeOffAirPort;
    public String LandingAirPort;

    public String getDurationTime() {
        return DurationTime;
    }

    public void setDurationTime(String DurationTime) {
        this.DurationTime = DurationTime;
    }
    public String TakeOffTime;
    public String LandingTime;
    public String DurationTime;

    public String getTakeOffAirPort() {
        return TakeOffAirPort;
    }

    public void setTakeOffAirPort(String TakeOffAirPort) {
        this.TakeOffAirPort = TakeOffAirPort;
    }

    public String getLandingAirPort() {
        return LandingAirPort;
    }

    public void setLandingAirPort(String LandingAirPort) {
        this.LandingAirPort = LandingAirPort;
    }

    public String getTakeOffTime() {
        return TakeOffTime;
    }

    public void setTakeOffTime(String TakeOffTime) {
        this.TakeOffTime = TakeOffTime;
    }

    public String getLandingTime() {
        return LandingTime;
    }

    public void setLandingTime(String LandingTime) {
        this.LandingTime = LandingTime;
    }

    public String getOutboundAirline() {
        return OutboundAirline;
    }

    public void setOutboundAirline(String OutboundAirline) {
        this.OutboundAirline = OutboundAirline;
    }
    public double getOutboundFlightPrice() {
        return OutboundFlightPrice;
    }

    public void setOutboundFlightPrice(double OutboundFlightPrice) {
        this.OutboundFlightPrice = OutboundFlightPrice;
    }

    @Override
    public int compareTo(FlightsDetail o) {
        return (int)(OutboundFlightPrice-o.getOutboundFlightPrice());
    }
    

    
}
