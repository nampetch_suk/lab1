/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.view;

import com.mycompany.fightreservation.domain.CostDetail;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Administrator
 */
public class CostList extends JPanel implements ListCellRenderer<CostDetail> {

    //private ImageIcon icon;
    
    public CostList(){
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
    }
    @Override
    public Component getListCellRendererComponent(JList list, 
            CostDetail value, int index, boolean isSelected, boolean cellHasFocus) {
            
       this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
       this.removeAll();
       
       
       String outboundCostName = value.getOutboundCostName();
       JPanel CostNamePanel = new JPanel();
       JLabel CostNameLabel = new JLabel(outboundCostName);
       CostNamePanel.add(CostNameLabel);
       CostNamePanel.setPreferredSize(new Dimension(200, 20));
       CostNamePanel.setMaximumSize(new Dimension(200, 20));
       CostNamePanel.setMinimumSize(new Dimension(200, 20));
       CostNameLabel.setFont(new Font("Courier New",Font.PLAIN,14));
       this.add(CostNamePanel);
       
       Component rigidArea1 = Box.createRigidArea(new Dimension(5,20));
       this.add(rigidArea1);
   
       
       
       double outBoundPrice = value.getOutboundPrice();
       JLabel price1 = new JLabel(String.valueOf(outBoundPrice));
       price1.setFont(new Font("Courier New",Font.PLAIN,14));
       price1.setPreferredSize(new Dimension(50,20));
       price1.setMaximumSize(new Dimension(50,20));
       price1.setMaximumSize(new Dimension(50,20));
       this.add(price1);
       
       return this;
    }
    
    
}
