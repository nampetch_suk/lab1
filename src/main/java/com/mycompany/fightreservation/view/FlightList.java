/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fightreservation.view;

import com.mycompany.fightreservation.domain.FlightsDetail;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EtchedBorder;

/**
 *
 * @author Administrator
 */
public class FlightList extends JPanel implements ListCellRenderer<FlightsDetail> {

    private ImageIcon icon;
    
    public FlightList(){
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    }
    
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
    }
    @Override
    public Component getListCellRendererComponent(JList list, 
            FlightsDetail value, int index, boolean isSelected, boolean cellHasFocus) {
            
       this.setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
       this.removeAll();
       
       double outBoundPrice = value.getOutboundFlightPrice();
       JLabel price1 = new JLabel(String.valueOf(outBoundPrice));
       price1.setFont(new Font("Courier New",Font.BOLD,20));
       price1.setForeground(Color.BLUE);
       price1.setPreferredSize(new Dimension(100,20));
       price1.setMaximumSize(new Dimension(100,20));
       price1.setMaximumSize(new Dimension(100,20));
       this.add(price1);
       
       Component rigidArea1 = Box.createRigidArea(new Dimension(10,10));
       this.add(rigidArea1);
       
       String outBoundAirline = value.getOutboundAirline();
       String takeoffAir = value.getTakeOffAirPort();
       String takeoffTime = value.getTakeOffTime();
       String landingAir = value.getLandingAirPort();
       String landingTime = value.getLandingTime();
       String durationTime = value.getDurationTime();
       
       ImageIcon temp = new ImageIcon(outBoundAirline+".jpg");
       icon = new ImageIcon(temp.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
       JPanel picPanel = new JPanel();
       JLabel picLabel = new JLabel(icon);
       picPanel.add(picLabel);

       picPanel.setPreferredSize(new Dimension(50, 50));
       picPanel.setMaximumSize(new Dimension(50, 50));
       picPanel.setMinimumSize(new Dimension(50, 50));
       this.add(picPanel);
       
       Component rigidArea2 = Box.createRigidArea(new Dimension(10, 10));
       this.add(rigidArea2);
       
       JPanel airLinePanel = new JPanel();
       JLabel airLineLabel = new JLabel(outBoundAirline);
       airLinePanel.add(airLineLabel);
       airLinePanel.setPreferredSize(new Dimension(200, 30));
       airLinePanel.setMaximumSize(new Dimension(200, 30));
       airLinePanel.setMinimumSize(new Dimension(200, 30));
       airLineLabel.setFont(new Font("Courier New",Font.BOLD,16));
       this.add(airLinePanel);
       
       Component rigidArea3 = Box.createRigidArea(new Dimension(10, 10));
       this.add(rigidArea3);
       
       JPanel takeOffAirPanel = new JPanel();
       JLabel takeOffAirLabel = new JLabel(takeoffAir);
       takeOffAirPanel.add(takeOffAirLabel);
       takeOffAirPanel.setPreferredSize(new Dimension(50, 20));
       takeOffAirPanel.setMaximumSize(new Dimension(50, 20));
       takeOffAirPanel.setMinimumSize(new Dimension(50, 20));
       takeOffAirLabel.setFont(new Font("Courier New",Font.BOLD,16));
       this.add(takeOffAirPanel);
       
       Component rigidArea4 = Box.createRigidArea(new Dimension(10, 10));
       this.add(rigidArea4);
       
       JPanel takeOffTimePanel = new JPanel();
       JLabel takeOffTimeLabel = new JLabel(takeoffTime);
       takeOffTimePanel.add(takeOffTimeLabel);
       takeOffTimePanel.setPreferredSize(new Dimension(50, 20));
       takeOffTimePanel.setMaximumSize(new Dimension(50, 20));
       takeOffTimePanel.setMinimumSize(new Dimension(50, 20));
       takeOffTimeLabel.setFont(new Font("Courier New",Font.ITALIC,16));
       this.add(takeOffTimePanel);
       
       Component rigidArea5 = Box.createRigidArea(new Dimension(10, 10));
       this.add(rigidArea5);
       
       JPanel landingAirPanel = new JPanel();
       JLabel landingAirLabel = new JLabel(landingAir);
       landingAirPanel.add(landingAirLabel);
       landingAirPanel.setPreferredSize(new Dimension(50, 20));
       landingAirPanel.setMaximumSize(new Dimension(50, 20));
       landingAirPanel.setMinimumSize(new Dimension(50, 20));
       landingAirLabel.setFont(new Font("Courier New",Font.BOLD,16));
       this.add(landingAirPanel);
       
       Component rigidArea6 = Box.createRigidArea(new Dimension(10, 10));
       this.add(rigidArea6);
       
       JPanel landingTimePanel = new JPanel();
       JLabel landingTimeLabel = new JLabel(landingTime);
       landingTimePanel.add(landingTimeLabel);
       landingTimePanel.setPreferredSize(new Dimension(50, 20));
       landingTimePanel.setMaximumSize(new Dimension(50, 20));
       landingTimePanel.setMinimumSize(new Dimension(50, 20));
       landingTimeLabel.setFont(new Font("Courier New",Font.ITALIC,16));
       this.add(landingTimePanel);
       
       Component rigidArea7 = Box.createRigidArea(new Dimension(10, 10));
       this.add(rigidArea7);
       
       JPanel durationTimePanel = new JPanel();
       JLabel durationTimeLabel = new JLabel(durationTime);
       durationTimePanel.add(durationTimeLabel);
       durationTimePanel.setPreferredSize(new Dimension(100, 20));
       durationTimePanel.setMaximumSize(new Dimension(100, 20));
       durationTimePanel.setMinimumSize(new Dimension(100, 20));
       durationTimeLabel.setFont(new Font("Courier New",Font.PLAIN,16));
       this.add(durationTimePanel);
       
       return this;
    }
    
    
}
